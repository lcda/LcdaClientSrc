#!/bin/bash

set -euo pipefail

for F in *.pug; do
	(pug --pretty "$F" && mv "$(basename "$F" .pug).html" "../../lcdagui.hak/$(basename "$F" .pug).xml")&
done

wait

echo "[Done]"
read
