#!/bin/bash

shopt -s nocasematch

FIFO=$(mktemp -t process_comm.XXXXXXXXXX)
mkfifo "$FIFO"
trap 'rm -f "$FIFO"' EXIT

AVAIL_THREADS="${NUMBER_OF_PROCESSORS:-4}"

for F in "$@"; do
	RUN=0

	if (( AVAIL_THREADS > 0 )); then
		AVAIL_THREADS=$((AVAIL_THREADS - 1))
		RUN=1
	fi

	if (( RUN == 0 )); then
		read x < "$FIFO"
	fi

	(
		OLDSIZE=$(stat -c '%s' "$F")
		if [[ "$F" == *.dds ]]; then
			convert "$F" -resize '512x512>' -define dds:compression=dxt5 "$F.tmp" && mv "$F.tmp" "$F" || rm -f "$F.tmp"
		elif [[ "$F" == *.tga ]]; then
			convert "$F" -resize '512x512>' "$F.tmp" && mv "$F.tmp" "$F" || rm -f "$F.tmp"
		else
			echo "ERROR: Unknown texture format: $F"
		fi
		NEWSIZE=$(stat -c '%s' "$F")

		printf '%-32s %dK -> %dK\n' "$F" $((OLDSIZE / 1000)) $((NEWSIZE / 1000))
		echo "$F" > "$FIFO"
	)&
done

wait