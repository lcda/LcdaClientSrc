#!/bin/bash
# ex Bonus Feat uncanny dodge: 12 800 X X
#     => echo "12\n800\n\n\n" | ./find-items-with-properties.sh

set -euo pipefail

FILTER=""

echo "========== Property ID =========="
echo "Index in itempropdef.2da"
read PROPID
if [ -n "$PROPID" ]; then
	if [ -n "$FILTER" ]; then
		FILTER="$FILTER and"
	fi
	FILTER="$FILTER .value.PropertyName.value == $PROPID"
fi

echo "========== Sub type =========="
echo "Index in the table defined in the SubTypeResRef column in itempropdef.2da"
echo "Leave empty for no sub type"
read SUBTYPE
if [ -n "$SUBTYPE" ]; then
	if [ -n "$FILTER" ]; then
		FILTER="$FILTER and"
	fi
	FILTER="$FILTER .value.Subtype.value == $SUBTYPE"
fi

echo "========== Cost value =========="
echo "Index in the table defined in iprp_costtable.2da at the index defined by the CostTableResRef column in itempropdef.2da"
echo "Leave empty for no cost value"
read COSTVALUE
if [ -n "$COSTVALUE" ]; then
	if [ -n "$FILTER" ]; then
		FILTER="$FILTER and"
	fi
	FILTER="$FILTER .value.CostValue.value == $COSTVALUE"
fi

echo "========== Param1 value =========="
echo "Index in the table defined in iprp_paramtable.2da at the index defined by the Param1ResRef column in itempropdef.2da"
echo "Leave empty for no param1 value"
read PARAMVALUE
if [ -n "$PARAMVALUE" ]; then
	if [ -n "$FILTER" ]; then
		FILTER="$FILTER and"
	fi
	FILTER="$FILTER .value.Param1Value.value == $PARAMVALUE"
fi

if command -v nproc > /dev/null; then
	PROC_MAX=$(( $(nproc) * 2 ))
else
	PROC_MAX=$(( ${NUMBER_OF_PROCESSORS:-8} * 2 ))
fi

echo "Starting search..."
echo "=============================="

CNT=0
for F in *.uti *.UTI; do
	while (( $(jobs -r | wc -l) >= PROC_MAX )); do
		wait -n
	done

	(
		DATA=$(nwn-gff -i "$F" -k json | jq '.value.PropertiesList.value[] | select('"$FILTER"')')
		if [ -n "$DATA" ]; then
			echo "$F"
		fi
	)&
	CNT=$(( CNT + 1 ))
done

echo "=============================="
echo "=> Searched $CNT item blueprints"

