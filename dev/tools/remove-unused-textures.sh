#!/bin/bash

set -euo pipefail
shopt -s nocasematch

for F in *.dds *.tga; do
	NAME="${F%.*}"
	(grep -qi "$NAME" *.mdb *.MDB || (echo "$F" && rm "$F"))&
done
wait