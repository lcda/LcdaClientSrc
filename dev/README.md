# Ressources pour développeurs

Ce dossier contient différents fichiers utiles pour les developpeurs de LCDA, mais inutiles aux joueurs.

# Installation

- Placer tous les fichiers du dossier `terrain` dans `C:\Program Files\Atari\Neverwinter Nights 2\NWN2Toolset\Terrain`
- Les autres fichiers sont automatiquement chargés par l'éditeur NWN2

