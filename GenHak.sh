# set -e

if [[ ! -d out ]]; then
	mkdir out
fi

for dir in *.hak; do
	if [[ -d $dir ]]; then
		echo "========> $dir"
		FILE="out/`basename $dir`"
		./nwn-erf create -o $FILE $dir
	fi
done
echo "[Entrée] pour quitter"
read
