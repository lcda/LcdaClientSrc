Ce repository contient les sources des fichiers que le client devra télécharger.
Ce sont des sources, et il ne doit donc pas y avoir de fichiers de type zip, hak, erf, mod, pwc...
Les fichiers doivent __obligatoirement__ être placés dans un des dossiers situés à la racine.

Les dossiers se terminant par .hak seront packés dans un fichier Hak, qui sera téléchargé dans le dossier Documents/Neverwinter Nights 2/hak.
Le dossier Lcda correspond au 'pwc' et sera téléchargé dans le dossier Documents/Neverwinter Nights 2/modules. (Pas de package pour le 'pwc')

---

Merci d'organiser vos fichiers dans les bons dossiers :

* lcda2da.hak: contient toutes les tables 2DA `*.2da`
* lcdamaterial.hak: Toutes les textures utilisées, sauf celles des modèles (créatures, plaçables, ...), ainsi que les icônes  `*.dds, *.bmp, *.tga, ...`
* lcdavfx.hak: Effets visuels `*.pfx, *.bbx, *.sef, ...`
* lcdamusic.hak: Musiques utilisées `*.bmu`
* lcdamdl.hak: __Ne pas modifier__
* lcdamdl2.hak: Modèles 3d de plaçables, créatures, equipement, ... ainsi que leur textures spécifiques `*.mdb, *.gr2, *.dds, *.bmp, *.tga, ...`
* lcdagui.hak: Fichiers de l'interface graphique: `ingamegui.ini, *.xml`
* music: Musiques ajoutées
* tlk: TalkTable, contient le Lcda.tlk
* dev: Fichiers nécessaires uniquement pour le développement du module (inutiles pour les joueurs)
* out: Dossier créé automatiquement via GenHack.sh, qui contient les fichiers haks générés via le contenu des dossier ci dessus